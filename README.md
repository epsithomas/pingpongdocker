[![pipeline status](https://gitlab.com/epsithomas/pingpongdocker/badges/master/pipeline.svg)](https://gitlab.com/epsithomas/pingpongdocker/commits/master)

GROUPE 7
Tancrède TIBAU
Thomas SORMONTE

DOCKERIZE L'APPLICATION:

BUILD :
se placer dans le répertoire app et saisir la commande suivante :
- docker build -t pingpong:latest .

LANCER :
se placer dans le répertoire app et saisir la commande suivante :
- docker run --rm -p 5000:5000 pingpong

DOCKERIZE LE TEST:

BUILD :
se placer dans le répertoire test et saisir la commande suivante :
- docker build -t test:latest .

LANCER :
se placer dans le répertoire test et saisir la commande suivante :
- docker run --rm --network=host -p 5000:5000 test